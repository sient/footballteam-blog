<?php

Route::group(['prefix' => 'auth', 'middleware' => 'api'], function () {
    Route::post('register', 'Auth\Controllers\RegisterController@apiRegister');
    Route::post('login', 'Auth\Controllers\LoginController@apiLogin');
});

Route::middleware(['auth:api'])->group(function () {
    Route::get('posts', 'HomeController@apiIndex');
});
