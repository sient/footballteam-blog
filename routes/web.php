<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

// AUTH ROUTES
Route::get('login', 'HomeController@login')->name('login-page');
Route::post('login', 'Auth\Controllers\LoginController@authenticate')->name('login');
Route::get('register', 'HomeController@register')->name('register-page');
Route::post('register', 'Auth\Controllers\RegisterController@register')->name('register');
Route::get('logout', 'Auth\Controllers\LoginController@logout')->name('logout');
Route::get('forgot-password', 'Auth\Controllers\ForgotPasswordController@index')->name('forgot-password');
Route::post('forgot-password', 'Auth\Controllers\ForgotPasswordController@sendResetLink')->name('send-reset-link');
Route::get('reset-password/{token}', 'Auth\Controllers\ResetPasswordController@index')->name('reset-password-page');
Route::post('reset-password/{token}', 'Auth\Controllers\ResetPasswordController@resetPassword')->name('reset-password');

Route::middleware(['editor'])->prefix('dashboard')->group(function () {
    // POSTS
    Route::prefix('post')->group(function () {
        Route::get('/', 'Admin\PostsController@index')->name('admin-index');
        Route::get('add', 'Admin\PostsController@create')->name('admin-create-post');
        Route::post('add', 'Admin\PostsController@store')->name('admin-store-post');
        Route::get('edit/{id}', 'Admin\PostsController@edit')->name('admin-edit-post');
        Route::post('edit/{id}', 'Admin\PostsController@update')->name('admin-update-post');
        Route::get('delete/{id}', 'Admin\PostsController@destroy')->name('admin-delete-post');
    });
});

Route::middleware(['admin'])->prefix('dashboard')->group(function () {
    // USERS
    Route::prefix('user')->group(function () {
        Route::get('/', 'Admin\UsersController@index')->name('admin-users');
        Route::get('add', 'Admin\UsersController@create')->name('admin-create-user');
        Route::post('add', 'Admin\UsersController@store')->name('admin-store-user');
        Route::get('edit/{id}', 'Admin\UsersController@edit')->name('admin-edit-user');
        Route::post('edit/{id}', 'Admin\UsersController@update')->name('admin-update-user');
        Route::get('delete/{id}', 'Admin\UsersController@destroy')->name('admin-delete-user');
    });
});
