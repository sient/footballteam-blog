@extends('dashboard.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-tabs" data-background-color="rose">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <ul class="nav nav-tabs">
                                    <li>
                                        <a href="{{ route('admin-create-post') }}">
                                            <i class="material-icons">add</i>
                                            Add new post
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th class="text-right"><i class="material-icons">settings</i></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $post)
                                    <tr>
                                        <td class="text-center">{{ $post->id }}</td>
                                        <td>{{ $post->title }}</td>
                                        <td>
                                            <img src="{{ $post->getImage() }}" style="width: 200px;">
                                        </td>
                                        <td class="td-actions text-right">
                                            <a class="btn btn-success" href="{{ route('admin-edit-post', $post->id) }}">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <a class="btn btn-danger" href="{{ route('admin-delete-post', $post->id) }}">
                                                <i class="material-icons">close</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center">
                            {{ $posts->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
