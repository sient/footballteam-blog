@extends('dashboard.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="rose">
                        <i class="material-icons">perm_identity</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Edit user</h4>
                        <form method="POST" action="{{ route('admin-update-user', $user->id) }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Name</label>
                                        <input type="text" name="name" class="form-control" minlength="3" maxlength="15" value="{{ $user->name }}" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input type="email" name="email" class="form-control" value="{{ $user->email }}" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Password</label>
                                        <input type="password" name="password" class="form-control" minlength="8">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <select class="selectpicker" data-style="select-with-transition" title="Role" name="role">
                                        <option disabled> Choose role</option>
                                        <option value="1" {{ $user->role == 1 ? 'selected' : '' }}>User </option>
                                        <option value="2" {{ $user->role == 2 ? 'selected' : '' }}>Editor</option>
                                        <option value="3" {{ $user->role == 3 ? 'selected' : '' }}>Administrator</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-rose pull-right">Edit</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
