@extends('dashboard.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-tabs" data-background-color="rose">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <ul class="nav nav-tabs">
                                    <li>
                                        <a href="{{ route('admin-create-user') }}">
                                            <i class="material-icons">add</i>
                                            Add new user
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"></h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th class="text-right"><i class="material-icons">settings</i></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td class="text-center">{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            @if($user->role == 1)
                                                User
                                            @elseif($user->role == 2)
                                                Editor
                                            @else
                                                Administrator
                                            @endif
                                        </td>
                                        <td class="td-actions text-right">
                                            <a class="btn btn-success" href="{{ route('admin-edit-user', $user->id) }}">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <a class="btn btn-danger" href="{{ route('admin-delete-user', $user->id) }}">
                                                <i class="material-icons">close</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center">
                            {{ $users->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
