<nav class="navbar navbar-primary navbar-transparent navbar-absolute">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Blog</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li {{ request()->is('/') ? 'class=active' : '' }}>
                    <a href="{{ route('home') }}">
                        <i class="material-icons">dashboard</i> Blog
                    </a>
                </li>
                @guest
                    <li {{ request()->is('register') ? 'class=active' : '' }}>
                        <a href="{{ route('register-page') }}">
                            <i class="material-icons">person_add</i> Register
                        </a>
                    </li>
                    <li {{ request()->is('login') ? 'class=active' : '' }}>
                        <a href="{{ route('login-page') }}">
                            <i class="material-icons">fingerprint</i> Login
                        </a>
                    </li>
                @else
                    @if(\Auth::user()->isAdmin() || \Auth::user()->isEditor())
                    <li {{ request()->is('login') ? 'class=active' : '' }}>
                        <a href="{{ route('admin-index') }}">
                            <i class="material-icons">widgets</i> Dashboard
                        </a>
                    </li>
                    @endif
                    <li>
                        <a href="{{ route('logout') }}">
                            <i class="material-icons">person_add</i> Logout
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
