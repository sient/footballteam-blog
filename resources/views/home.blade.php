@extends('layouts.app')

@section('content')
    <div class="full-page pricing-page" data-image="/assets/img/bg-pricing.jpeg">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h2 class="title">Posts</h2>
                    </div>
                </div>
                <div class="row">
                    @foreach($posts as $post)
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content">
                                    <h4 class="card-title">{{ $post->title }}</h4>
                                </div>
                                <div style="text-align: center;">
                                    <img src="{{ $post->getImage() }}" alt="" style="height: auto; width: 200px;">
                                </div>
                                <div class="card-footer">
                                    <p>{{ $post->text }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="text-center">
                        {{ $posts->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
