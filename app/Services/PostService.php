<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostService
{
    public function uploadImage(Request $request, $path)
    {
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = Str::random(20) . '.' . $image->getClientOriginalExtension();

            $image->storeAs($path, $name);

            return $path . '/' . $name;
        }

        return null;
    }
}
