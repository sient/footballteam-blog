<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\Jobs\SendVerificationEmail;

class UserVerificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param UserCreated $event
     */
    public function handle(UserCreated $event)
    {
        SendVerificationEmail::dispatch($event);
    }
}
