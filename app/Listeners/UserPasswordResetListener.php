<?php

namespace App\Listeners;

use App\Events\ResetPassword;
use App\Jobs\SendPasswordResetEmail;

class UserPasswordResetListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param ResetPassword $event
     */
    public function handle(ResetPassword $event)
    {
        SendPasswordResetEmail::dispatch($event);
    }
}
