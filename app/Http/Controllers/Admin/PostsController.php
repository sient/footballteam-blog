<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Post;
use App\Services\PostService;

class PostsController extends Controller
{
    private $post;
    private $postService;

    /**
     * PostsController constructor.
     *
     * @param Post $post
     * @param PostService $postService
     */
    public function __construct(Post $post, PostService $postService)
    {
        $this->post = $post;
        $this->postService = $postService;

        $this->middleware('editor');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->post->paginate(10);

        return view('dashboard.posts.index')->with([
            'posts' => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.posts.create');
    }

    /**
     * @param PostRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PostRequest $request)
    {
        $data = $request->validated();

        if ($request->hasFile('image')) {
            $data['image'] = $this->postService->uploadImage($request, 'public/images');

            $this->post->create($data);

            return redirect()->route('admin-index');
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $post = $this->post->where('id', $id)->firstOrFail();

        return view('dashboard.posts.edit')->with([
            'post' => $post
        ]);
    }

    /**
     * @param PostRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PostRequest $request, $id)
    {
        $post = $this->post->where('id', $id)->firstOrFail();
        $data = $request->validated();

        if ($request->hasFile('image')) {
            $data['image'] = $this->postService->uploadImage($request, 'public/images');
        } else {
            $data['image'] = $post->image;
        }

        $post->update($data);

        return redirect()->route('admin-index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->post->where('id', $id)->firstOrFail();
        $post->delete();

        return redirect()->route('admin-index');
    }
}
