<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param array $data
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function success($data = [], $status = 200)
    {
        return response()->json($data, $status);
    }

    /**
     * @param array $message
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function error($message = [], $status = 500)
    {
        is_array($message) ? $message = null : [$message];
        return response()->json($message, $status);
    }
}
