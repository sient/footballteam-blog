<?php

namespace App\Http\Controllers;

use App\Post;

class HomeController extends Controller
{
    private $post;

    /**
     * HomeController constructor.
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->post->paginate(10);

        return view('home')->with([
            'posts' => $posts
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiIndex()
    {
        $posts = $this->post->paginate(10);

        return $this->success(['data' => $posts]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login()
    {
        return view('auth.login');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register()
    {
        return view('auth.register');
    }
}
