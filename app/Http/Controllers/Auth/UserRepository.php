<?php

namespace App\Http\Controllers\Auth;

use App\Events\ResetPassword;
use App\Events\UserCreated;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Str;

class UserRepository {

    use AuthenticatesUsers;

    protected $user;

    /**
     * UserRepository constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function firstOrFail($id)
    {
        return $this->user->where('id', $id)->firstOrFail();
    }

    /**
     * @param $email
     * @return mixed
     */
    public function findByEmail($email)
    {
        return $this->user->where('email', $email)->firstOrFail();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $data['password'] = Hash::make($data['password']);
        $user = $this->user->create($data);

        event(new UserCreated($user));

        return $user;
    }

    /**
     * @param Request $request
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
    }

    /**
     * @param $data
     * @param $userId
     * @return mixed
     */
    public function update($data, $userId)
    {
        $user = $this->firstOrFail($userId);

        if ($this->checkUsers($data['email'], $user)) {

            if ($data['password']) {
                $data['password'] = Hash::make($data['password']);
            } else {
                unset($data['password']);
            }

            $user->update($data);

            return $user;
        }
    }

    /**
     * @param $password
     * @param User $user
     * @param $token
     * @return User
     */
    public function resetPassword($password, User $user, $token)
    {
        $token = DB::table('password_resets')->where([
            ['email', $user->email],
            ['token', $token]
        ]);

        if ($token->first()) {

            $data['password'] = Hash::make($password);
            $user->update($data);
            $token->delete();

            return $user;
        }
    }

    /**
     * @param $email
     */
    public function forgotPassword($email)
    {
        $token = DB::table('password_resets')->where('email', $email)->first();

        if (!$token) {
            DB::table('password_resets')->insert([
                'email'      => $email,
                'token'      => Str::random(60),
                'created_at' => Carbon::now()
            ]);

            $token = DB::table('password_resets')->where('email', $email)->first();
        }

        event(new ResetPassword($email, $token->token));
    }

    /**
     * @param $email
     * @param $user
     * @return bool
     */
    private function checkUsers($email, $user)
    {
        $checkEmail = $this->user->where('email', $email)->first();

        if ($checkEmail) {
            if ($checkEmail->id == $user->id) {
                return true;
            }

            return false;
        }

        return true;
    }
}
