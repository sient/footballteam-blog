<?php

namespace App\Http\Controllers\Auth\Controllers;

use App\Http\Controllers\Auth\UserRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    private $request;
    private $repository;

    /**
     * ForgotPasswordController constructor.
     *
     * @param Request $request
     * @param UserRepository $repository
     */
    public function __construct(Request $request, UserRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
        $this->middleware('guest');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('auth.forgot-password');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendResetLink()
    {
        $rules = [
            'email' => 'required|string|max:255'
        ];
        $this->validate($this->request, $rules);

        $email = $this->request->get('email');

        $this->repository->findByEmail($email);
        $this->repository->forgotPassword($email);

        return redirect()->route('home');
    }
}
