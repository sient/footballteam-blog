<?php

namespace App\Http\Controllers\Auth\Controllers;

use App\Http\Controllers\Auth\UserRepository;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResetPasswordController extends Controller
{
    private $user;
    private $request;
    private $repository;

    public function __construct(User $user, Request $request, UserRepository $repository)
    {
        $this->user = $user;
        $this->request = $request;
        $this->repository = $repository;
        $this->middleware('guest');
    }

    /**
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index($token)
    {
        $rules = [
            'email' => 'required|email|max:191|exists:users,email'
        ];
        $this->validate($this->request, $rules);

        $token = DB::table('password_resets')->where([
            ['email', $this->request->get('email')],
            ['token', $token]
        ])->first();

        if ($token) {
            return view('auth.reset-password')->with([
                'token' => $token->token,
                'email' => $token->email
            ]);
        }

        return redirect()->route('home');
    }

    /**
     * @param $token
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function resetPassword($token)
    {
        $rules = [
            'email'    => 'required|email|max:191|exists:users,email',
            'password' => 'required|string|min:8|max:191'
        ];
        $this->validate($this->request, $rules);

        $email = $this->request->get('email');

        $user = $this->repository->findByEmail($email);
        $this->repository->resetPassword($this->request->get('password'), $user, $token);

        return redirect()->route('login-page');
    }
}
