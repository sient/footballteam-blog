<?php

namespace App\Http\Controllers\Auth\Controllers;

use App\Http\Controllers\Auth\UserRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    private $repository;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * RegisterController constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('guest');
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiRegister(RegisterRequest $request)
    {
        $data = $request->validated();

        try {
            $user = $this->create($data);
        } catch (\Exception $e) {
            return $this->error('Something went wrong');
        }

        return $this->registered($user);
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterRequest $request)
    {
        $data = $request->validated();

        try {
            $this->create($data);
        } catch (\Exception $e) {
            return redirect($this->redirectTo);
        }

        return redirect()->route('login-page');
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function create($data)
    {
        return $this->repository->create($data);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    protected function registered(User $user)
    {
        $token = $user->createToken('api')->accessToken;

        return $this->success(['data' => $token]);
    }
}
