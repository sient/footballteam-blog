<?php

namespace App\Jobs;

use App\Mail\SendVerification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendVerificationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $event;

    /**
     * SendEmail constructor.
     *
     * @param $event
     */
    public function __construct($event)
    {
        $this->event = $event;
    }

    public function handle()
    {
        Mail::to($this->event->user->email)->send(new SendVerification());
    }
}
