<?php

namespace App\Jobs;

use App\Mail\SendPasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendPasswordResetEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $event;

    /**
     * SendEmail constructor.
     *
     * @param $event
     */
    public function __construct($event)
    {
        $this->event = $event;
    }

    public function handle()
    {
        Mail::to($this->event->email)->send(new SendPasswordReset($this->event->email, $this->event->token));
    }
}
