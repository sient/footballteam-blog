<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    protected $fillable = [
        'title',
        'image',
        'text'
    ];

    /**
     * @return mixed
     */
    public function getImage()
    {
        return Storage::url($this->image);
    }
}
