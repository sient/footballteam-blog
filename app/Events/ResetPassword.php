<?php

namespace App\Events;

class ResetPassword
{
    public $email;
    public $token;

    public function __construct($email, $token)
    {
        $this->email = $email;
        $this->token = $token;
    }

}
