<?php

namespace App\Events;

use App\User;

class UserCreated
{
    public $user;

    /**
     * UserCreated constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

}
